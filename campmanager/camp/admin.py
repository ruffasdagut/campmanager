from django.contrib import admin
from camp.models import *

admin.site.register(Person)
admin.site.register(Group)
admin.site.register(School)
admin.site.register(Grade)
admin.site.register(Camper)
admin.site.register(Guarantor)