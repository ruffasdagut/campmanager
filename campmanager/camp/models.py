from django.db import models

# Create your models here.

class Person(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    register_date = models.DateTimeField(auto_now_add=True,blank=True)
    
    def __unicode__(self):
        return self.first_name + " " + self.last_name

class Address(models.Model):
    person = models.ForeignKey(Person)
    street = models.CharField(max_length=300)
    city = models.CharField(max_length=200)
    state = models.CharField(max_length=2)
    zip = models.IntegerField()
    
    def __unicode__(self):
        return self.person + " " +  self.street + " " + \
               self.city + ", " + self.state + " " + self.zip
    
class Group(models.Model):
    name = models.CharField(max_length=200)
    
    def __unicode__(self):
        return self.name
    
class School(models.Model):
    name = models.CharField(max_length=200)
    
    def __unicode__(self):
        return self.name
    
class Grade(models.Model):
    name = models.CharField(max_length=20)
    
    def __unicode__(self):
        return self.name
    
class Camper(models.Model):
    person = models.ForeignKey(Person)
    age = models.IntegerField()
    group = models.ForeignKey(Group)
    school = models.ForeignKey(School)
    grade = models.ForeignKey(Grade)
    
    def __unicode__(self):
        return self.person
    
class Guarantor(models.Model):
    person = models.ForeignKey(Person)
    campers = models.ManyToManyField(Camper)
    
    def __unicode__(self):
        return self.person
    
    


    